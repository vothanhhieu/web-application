from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'index.html')

def sanpham(request):
    return render(request, 'shop.html')

def login(request):
    return render(request, 'Login.html')
