from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('',views.home),
    path('shop.html', views.sanpham),
    path('Login.html', views.login)
]